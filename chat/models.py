from django.db import models
from django.contrib.auth.models import User
# Create your models here.
# class user2(models.Model):
#     User2 = models.ForeignKey(User1, on_delete=models.CASCADE)
#     UserName=models.CharField(max_length=300)
#     FullName=models.user1.FullName

class Send(models.Model):
    id = models.AutoField(primary_key=True)
    send = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    # pub_date = models.DateTimeField()
    # votes_total = models.IntegerField(default=1)
    def __str__(self):
        return self.title
    # def summary(self):
    #     return self.body[:100]
    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')
