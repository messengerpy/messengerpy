from chat import views,urls
from django.urls import path
from django.views import generic
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
    path('chat1',views.chatview,name='chat1'),
    path('send', views.send, name='send'),
    # path('detail', views.detail, name='detail'),
    path('<int:send_id>',views.detail, name='detail'),


]
urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
