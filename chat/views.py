from django.shortcuts import render, redirect, get_object_or_404
from home import models
from .models import Send
from django.contrib.auth.decorators import login_required
from django.utils import timezone
# Create your views here.

# def myfunc(request):
#     x=doccument.getElementById("my")
#     if x.style.display=="none":
#         x.style.display="block"

def ListView(request):
    modelhome=Send.objects
    return render(request,'job3/chat1.html',{'modelhome':modelhome})

def chatview(request):
    sends=Send.objects
    return render(request,'job3/chat1.html',{'sends':sends})

@login_required
def send(request):
    if request.method == 'POST':
        if request.POST['title'] :
            send = Send()
            send.title = request.POST['title']
            # send.pub_date = timezone.datetime.now()
            send.send = request.user
            send.save()
            return redirect('chat1')
        # / job3 / ' + str(send.id)
        else:
            return render(request, 'job3/chat1.html',{'error':'تمام فیلد ها باید تکمیل شوند.'})
    else:
        return render(request, 'job3/chat1.html')

def detail(request, send_id):
    send = get_object_or_404(Send, pk=send_id)
    return render(request, 'job3/detail.html',{'send':send})

def usename(request):
        modelhome=models.form.objects.all()
        return render(request,'job3/chat1.html',{'name':modelhome.first_name})
