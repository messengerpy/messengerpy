from django.contrib import admin
from . import models
# Register your models here.
@admin.register(models.form)
class formAdmin(admin.ModelAdmin):
    list_display=('UserName','first_name','last_name','email','Age','id')
admin.site.register(models.channel)
class channelAdmin(admin.ModelAdmin):
    list_display=('id','Name','created','users')
admin.site.register(models.Group)
class GroupAdmin(admin.ModelAdmin):
    list_display=('id','Name','created','users')
    # OR
    # from . import models
    # @admin.register(models.user1)
