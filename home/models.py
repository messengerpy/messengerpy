from django.db import models
import django
import datetime
from django.contrib.auth.models import User
# Create your models here.
class form(models.Model):
    id=models.IntegerField(primary_key=True)
    UserName=models.CharField(max_length=300)
    first_name=models.CharField(max_length=300)
    last_name=models.CharField(max_length=300)
    email=models.EmailField()
    Age=models.IntegerField()
    # Gender=models.SET()
class channel(models.Model):
    id=models.IntegerField(primary_key=True)
    Name=models.CharField(max_length=300)
    created=models.DateTimeField(default=django.utils.timezone.now)
    users=models.IntegerField(default=0)
class Group(models.Model):
    id=models.IntegerField(primary_key=True)
    Name=models.CharField(max_length=300)
    created=models.DateTimeField(default=django.utils.timezone.now)
    users=models.IntegerField(default=0)
