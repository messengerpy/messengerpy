# Generated by Django 2.2.5 on 2020-09-14 01:52

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='channel',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('Name', models.CharField(max_length=300)),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('users', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('Name', models.CharField(max_length=300)),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('users', models.IntegerField(default=0)),
            ],
        ),
    ]
