from django.urls import path, include
from accounts import views,urls
from django.views import generic
# appname='home'
urlpatterns = [
    path('signup',views.signup, name='signup'),
    path('login',views.login, name='login'),
    path('logout',views.logout, name='logout'),
]
