from django.contrib.auth.models import User
from django.contrib import auth
from home import views,urls,models
from django.shortcuts import render, redirect

def signup(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.get(username=request.POST['username'])
                return render(request, 'accounts/signup.html',
                {'error':'this username has been created before'})
            except User.DoesNotExist:
                user = User.objects.create_user(username=request.POST['username']
                ,password=request.POST['password1'],first_name = request.POST['first_name']
                ,last_name=request.POST['last_name'],email = request.POST['email'])
                auth.login(request,user)
                return redirect('chat1')
        else:
            return render(request, 'accounts/signup.html', {'error':'رمز عبور ها باید یکی باشند '})
    else:
        # User wants to enter info
        return render(request, 'accounts/signup.html')
def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['username'],password=request.POST['password1'])
        if user is not None:
            auth.login(request, user)
            return redirect('chat1')
        else:
            return render(request, 'accounts/login.html',{'error':'نام کاربری یا رمز عبور اشتباه است '})
    else:
        return render(request, 'accounts/login.html')

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('ListView')
